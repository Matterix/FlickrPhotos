# FlickrPhotos

# Fragments

## BaseFragment

Alle andre fragments extender BaseFragment, hvis nogle fragments skal bruge de samme funktionaliteter kan de laves her --> God genbrug

## MapFragment
**void setLocationRequestParameters()

Angiv parametre som location skal bruge, hvor ofte man skal hente lokationsdata + batteri prioritet

**void requestLocation()

Loop lokationsopdateringer med locationRequest og callback som henter resultater og gemmer i sharedPreference


## MyImagesFragment

REST kald virkede ikke, jeg har uploadet to billeder, de blev ikke fanget i kaldet, heller ikke i Flickr API explorer. Jeg undlod at lave denne del af opgaven --> Hvis jeg havde fået REST kald til at virke, havde jeg sat den op på samme måde som PublicImagesFragment -> Fuldstændig samme logik der skal bruges og samme opsætning bare med ens egne billeder i stedet. 

## MyProfileFragment

**void readProfileValues()**

Får profil data når man går til siden, indlæser denne profildata ind i respektive TextViews.

**void getNearbyImages()**
 
Henter billeder ned som er i nærheden og laver en markør ud fra deres lat,long

**void createMapMarkersFromList()**

Opretter markører ud fra en liste --> Bruges i getNearbyImages()
Gemmer markørdata i marker.SetTag(MarkerItem)

**markerClickListener**

Åbner fullscreen dialog ud fra hvilken markør der er trykket på. Markør data er hentet fra marker.getTag()


## PublicImagesFragment

**void loadPhotos**

Indlæser billeder fra Flickr og tilføjer dem til en liste

**private class PhotoHolder**
  
Definerer hvilket ImageView billeder skal sættes ind i

**PhotoAdapter**

Bindes sammen med PhotoHolder --> Indlæser billeder ind i ImageView via Picasso 
Picasso.get().load(galleryItem.getThumbnail).placeholder(R.drawable.ic_placeholder).into(photoHolder.mGalleryItemImageView)

**void setupAdapter()**

Sætter adapter på RecyclerView

**void openFullscreenImage()

Åbner fullscreen dialog ud fra hvilket billede der er trykket på.


# Handlers

## FragmentHandler

**void startTransactionNoAnimationWithBackstack(BaseFragment)**

Bruges til at skifte fragment, uden animation og bliver tilføjet til backstacken.

**void startTransactionNoBackstack(BaseFragment)**

Bruges til at skifte fragment, uden animation og bliver ikke tilføjet til backstacken.

**void startTransactionAnimatedWithBackstack(BaseFragment)**

Bruges til at skifte fragment, med animation og bliver tilføjet til backstacken.

**void startTransactionAnimatedNoBackstack(BaseFragment)**

Bruges til at skifte fragment, med animation, og bliver ikke tilføjet til backstacken.

**void popBackstack()**

Fjerner øverste fragment = Gå tilbage til forrige side.

**List<Fragment> getFragments()**
  
Liste af fragments der er i FragmentManager

-->Disse er lavet med henblik på fremtidig brug, f.eks. hvis der nemt skal tilføjes animation til fragment transition
  
## FlickrClientApp


**getRestClient()**

Bruges til at etablere forbindelse til client.

## FlickrClientHandler** 

Indeholder basis kode for REST kald --> url + consumer_key + consumer_secret + callback_url, samt de forskellige REST kald der laves i programmet.

**void getPublicImages(AsyncHttpResponseHandler)**

Sender REST kald --> når man kalder den modtager man JSON respons --> method=flickr.photos.getRecent

**void getMyImages(AsyncHttpResponseHandler)**

Sender REST kald --> når man kalder den modtager man JSON respons --> method=flickr.activity.userPhotos

**void getMyProfile(AsyncHttpResponseHandler)**

Sender REST kald --> når man kalder den modtager man JSON respons --> method=flickr.profile.getProfile

## MapHandler

**static MapHandler getMapsHandler()

Singleton instans af klassen til at håndtere maps funktioner

**void getMapSettings(Context, Googlemap)**

Sætter settings på map her --> nemt at genbruge hvis map skal vises andre steder

**void moveCameraTo(GoogleMap, LatLng, Float)**

Gå direkte til et punkt på kortet --> uden animation --> Float = zoom level 1-20 

**void animateCameraTo(GoogleMap, LatLng, Float)**

Gå direkte til et punkt på kortet --> med animation --> Float = zoom level 1-20 

## SharedPrefHandler

**String getString(String)**

Bruges til at hente strings gemt i sharedPreference.

**void saveString(String,String)

Bruges til at gemme strings i sharedPreference.


# Models

## GalleryItem

Billede objekt med tilsvarende attributer der skal bruges i forhold til REST

## ProfileItem

Profil objekt med tilsvarende attributer der skal bruges i forhold til REST

## MarkerItem

Samme objekt som GalleryItem (Måske skulle jeg have brugt inheritance) --> Tilføjet Double latitude, Double longitude


# Utilities

## RecyclerItemClickListener

RecyclerView har pr. default ikke en onClickListener --> Dette er lavet for at håndtere disse via touch events
void onItemClick(View, int)
void onLongItemClick(View, int)


# Activities

## AuthenticationActivity

**void onLoginSuccess()**

Før brugeren videre til MainActivity

**void onLoginFailure()**

Skriv besked ud til brugeren via toast

**void onBackPressed()**

Gør sådan at brugeren ikke kan bruge tilbage knap når man er i dette view.

## MainActivity

**void getUserId()**

Hent brugerId ned fra Flickr og indlæs profilen udfra det brugerId som der hentes ned

**void initializeNavigationDrawer()**

Sætter navigationDrawer op inklusiv animationer og clickListener

**boolean onNavigationItemSelected(MenuItem)**

Switch med cases i forhold til de forskellige fragments man kan gå til. OnClick --> Gå til det fragment man trykkede på.

**void loadProfile()**

Indlæs profilen fra Flickr ud fra brugerId man har fået i getUserId() --> Lav ProfileItem ud fra dette --> Send videre senere

**void drawerAnimation()**

Lav drawerAnimation så når man trækker ud fra siden bliver menu ikon lavet om til en pil

**void removeSplashScreen()**

Hvis splashscreen er fremme --> så fjern den

**void showSplashSCreen()**

Åbner splashScreenDialog --> Fjernes efter 2,5 sekunder.

**void requestLocationPermission()**

Spørg om location permissions når det er påkrævet i forhold til kortet.









