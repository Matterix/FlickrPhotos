package dk.lundudvikling.flickrphotos.Fragments;

import android.support.v4.app.Fragment;

import dk.lundudvikling.flickrphotos.MainActivity;

public class BaseFragment extends Fragment {

    private MainActivity mainActivity;

    public BaseFragment(){
        super();
    }

    public MainActivity getMainActivity(){
        if (mainActivity == null)
            mainActivity = (MainActivity) getActivity();
        return mainActivity;
    }
}
