package dk.lundudvikling.flickrphotos.Fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import dk.lundudvikling.flickrphotos.Handlers.FlickrClientApp;
import dk.lundudvikling.flickrphotos.Handlers.FlickrClientHandler;
import dk.lundudvikling.flickrphotos.Utilities.RecyclerItemClickListener;
import dk.lundudvikling.flickrphotos.Model.GalleryItem;
import dk.lundudvikling.flickrphotos.R;

public class PublicImagesFragment extends BaseFragment{

    @BindView(R.id.fragment_public_images_recyclerview) RecyclerView mPhotoRecyclerView;
    @BindString(R.string.loading_images) String loadingImagesString;
    @BindString(R.string.loading_please_wait) String pleaseWaitString;

    private ArrayList<GalleryItem> mPublicPhotos = new ArrayList<>();
    private FlickrClientHandler client;
    private ProgressDialog progress;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_public_images, container, false);
        ButterKnife.bind(this, rootView);
        mPhotoRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle args = getArguments();
        if (args != null){
            if (args.getBoolean("shouldProgressShow")){
                progress = new ProgressDialog(getContext());
                progress.setTitle(loadingImagesString);
                progress.setMessage(pleaseWaitString);
                progress.setCancelable(false);
                progress.show();
            }
        }
        setupAdapter();
        client = FlickrClientApp.getRestClient();
        mPhotoRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), mPhotoRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                openFullscreenImage(position);
            }
            @Override
            public void onLongItemClick(View view, int position) {
                //Mulighed for at favorite photo her eventuelt?
            }
        }));
        loadPhotos();
    }

    public void openFullscreenImage(int position){
        final Dialog fullscreenImageDialog = new Dialog(getContext(),android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        fullscreenImageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        fullscreenImageDialog.setCancelable(false);
        fullscreenImageDialog.setContentView(R.layout.fullscreen_image);
        Button btnClose = fullscreenImageDialog.findViewById(R.id.btnIvClose);
        ImageView ivPreview = fullscreenImageDialog.findViewById(R.id.iv_preview_image);
        if (mPublicPhotos.get(position).getLargeImage() != null){
            Picasso.get().load(mPublicPhotos.get(position).getLargeImage()).into(ivPreview);
        }else{
            Picasso.get().load(mPublicPhotos.get(position).getThumbnail()).into(ivPreview);
            Toast.makeText(getContext(), "Full size image fandtes ikke", Toast.LENGTH_SHORT).show();
        }
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                fullscreenImageDialog.dismiss();
            }
        });
        fullscreenImageDialog.show();
    }

    private void loadPhotos() {
        client.getPublicImages(new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers,
                                  JSONObject json) {
                try {
                    JSONArray photos = json.getJSONObject("photos").getJSONArray("photo");
                    for (int i = 0; i < photos.length(); i++) {
                        JSONObject photoJsonObject = photos.getJSONObject(i);
                        GalleryItem item = new GalleryItem();
                        item.setId(photoJsonObject.getString("id"));
                        item.setCaption(photoJsonObject.getString("title"));
                        if (photoJsonObject.has("url_s")) {
                            item.setThumbnail(photoJsonObject.getString("url_s"));
                        }
                        if (photoJsonObject.has("url_z")){
                            item.setLargeImage(photoJsonObject.getString("url_z"));
                        }
                        mPublicPhotos.add(item);
                    }
                    if (progress != null){
                        progress.dismiss();
                    }
                    setupAdapter();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(getContext(), errorResponse.toString(), Toast.LENGTH_SHORT).show();
            }

        });
    }

    private class PhotoHolder extends RecyclerView.ViewHolder {
        private ImageView mGalleryItemImageView;
        public PhotoHolder(View itemView) {
            super(itemView);
            mGalleryItemImageView = itemView.findViewById(R.id.gallery_item_iv);
        }
    }

    private class PhotoAdapter extends RecyclerView.Adapter<PhotoHolder> {
        private ArrayList<GalleryItem> mGalleryItems;

        public PhotoAdapter(ArrayList<GalleryItem> galleryItems) {
            mGalleryItems = galleryItems;
        }
        @Override
        public PhotoHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View view = inflater.inflate(R.layout.gallery_item, viewGroup, false);
            return new PhotoHolder(view);
        }
        @Override
        public void onBindViewHolder(PhotoHolder photoHolder, int position) {
            GalleryItem galleryItem = mGalleryItems.get(position);
            Picasso.get()
                    .load(galleryItem.getThumbnail())
                    .into(photoHolder.mGalleryItemImageView);
        }
        @Override
        public int getItemCount() {
            return mGalleryItems.size();
        }
    }

    private void setupAdapter() {
        if (isAdded()) {
            mPhotoRecyclerView.setAdapter(new PhotoAdapter(mPublicPhotos));
        }
    }
}
