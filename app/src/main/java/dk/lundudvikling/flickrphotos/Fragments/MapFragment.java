package dk.lundudvikling.flickrphotos.Fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;

import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import dk.lundudvikling.flickrphotos.Handlers.FlickrClientApp;
import dk.lundudvikling.flickrphotos.Handlers.FlickrClientHandler;
import dk.lundudvikling.flickrphotos.Handlers.MapHandler;
import dk.lundudvikling.flickrphotos.Handlers.SharedPrefHandler;
import dk.lundudvikling.flickrphotos.Model.GalleryItem;
import dk.lundudvikling.flickrphotos.Model.MarkerItem;
import dk.lundudvikling.flickrphotos.R;

public class MapFragment extends BaseFragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    private FlickrClientHandler client;
    private SharedPrefHandler mPrefHandler;
    private ArrayList<MarkerItem> nearbyPhotos;
    private Marker myMarker;
    // TODO: 21-06-2018 SPØRG OM PERMISSION!

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        setLocationRequestParameters();
        requestLocation();
        client = FlickrClientApp.getRestClient();
        mPrefHandler = new SharedPrefHandler(getContext());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        MapHandler.getMapsHandler().getMapSettings(getContext(), mMap);

        //Hvis koordinater ikke findes --> Zoom ind på Danmark niveau
        if (mPrefHandler.getString("lat") == null){
            MapHandler.getMapsHandler().moveCameraTo(mMap, new LatLng(55.390613, 10.437439), 6.7f);
        }else{
            Double latCoordinate = Double.parseDouble(mPrefHandler.getString("lat"));
            Double longCoordinate = Double.parseDouble(mPrefHandler.getString("long"));
            MapHandler.getMapsHandler().moveCameraTo(mMap, new LatLng(latCoordinate, longCoordinate), 13.5f);
        }
        getNearbyImages();

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                MarkerItem item = (MarkerItem) (marker.getTag());
                final Dialog fullscreenImageDialog = new Dialog(getContext(),android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
                fullscreenImageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                fullscreenImageDialog.setCancelable(false);
                fullscreenImageDialog.setContentView(R.layout.fullscreen_image);
                Button btnClose = fullscreenImageDialog.findViewById(R.id.btnIvClose);
                ImageView ivPreview = fullscreenImageDialog.findViewById(R.id.iv_preview_image);
                if (item.getLargeImage() != null){
                    Picasso.get().load(item.getLargeImage()).into(ivPreview);
                }else{
                    Picasso.get().load(item.getThumbnail()).into(ivPreview);
                }
                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        fullscreenImageDialog.dismiss();
                    }
                });
                fullscreenImageDialog.show();
                return false;
            }
        });
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    public void setLocationRequestParameters() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(500);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    public void requestLocation() {
        if (mFusedLocationClient != null) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
                mPrefHandler.saveString("lat", String.valueOf(location.getLatitude()));
                mPrefHandler.saveString("long", String.valueOf(location.getLongitude()));
            }
        }
    };


    public void getNearbyImages() {
        nearbyPhotos = new ArrayList<>();
        client.getNearbyImages(new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers,
                                  JSONObject json) {
                try {
                    JSONArray photos = json.getJSONObject("photos").getJSONArray("photo");
                    for (int i = 0; i < photos.length(); i++) {
                        JSONObject photoJsonObject = photos.getJSONObject(i);
                        MarkerItem item = new MarkerItem();
                        item.setId(photoJsonObject.getString("id"));
                        item.setTitle(photoJsonObject.getString("title"));
                        if (photoJsonObject.has("url_s")) {
                            item.setThumbnail(photoJsonObject.getString("url_s"));
                        }
                        if (photoJsonObject.has("url_z")){
                            item.setLargeImage(photoJsonObject.getString("url_z"));
                        }
                        item.setLatitude(photoJsonObject.getDouble("latitude"));
                        item.setLongitude(photoJsonObject.getDouble("longitude"));
                        nearbyPhotos.add(item);
                    }
                    createMapMarkersFromList(nearbyPhotos);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("debug", e.toString());
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(getContext(), errorResponse.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void createMapMarkersFromList(ArrayList<MarkerItem> markerItems){
        for (MarkerItem item : markerItems){
            myMarker = mMap.addMarker(new MarkerOptions()
            .position(new LatLng(item.getLatitude(), item.getLongitude()))
            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
            myMarker.setTag(item);
        }
    }
}