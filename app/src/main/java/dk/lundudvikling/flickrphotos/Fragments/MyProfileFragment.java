package dk.lundudvikling.flickrphotos.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dk.lundudvikling.flickrphotos.AuthenticationActivity;
import dk.lundudvikling.flickrphotos.Handlers.FlickrClientApp;
import dk.lundudvikling.flickrphotos.Handlers.FlickrClientHandler;
import dk.lundudvikling.flickrphotos.Handlers.SharedPrefHandler;
import dk.lundudvikling.flickrphotos.Model.ProfileItem;
import dk.lundudvikling.flickrphotos.R;

public class MyProfileFragment extends BaseFragment {

    FlickrClientHandler client;
    @BindView(R.id.tv_first_name_display) TextView tvFirstName;
    @BindView(R.id.tv_last_name_display) TextView tvLastName;
    @BindView(R.id.tv_city_display) TextView tvCity;
    @BindView(R.id.tv_country_display) TextView tvCountry;
    @BindView(R.id.tv_hometown_display) TextView tvHometown;
    @BindView(R.id.tv_occupation_display) TextView tvOccupation;
    @BindView(R.id.tv_profile_description_display) TextView tvProfileDescription;
    private SharedPrefHandler mPrefHandler;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_profile, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        client = FlickrClientApp.getRestClient();
        mPrefHandler = new SharedPrefHandler(getContext());
        readProfileValues();
    }

    private void readProfileValues(){
        Bundle args = getArguments();
        ProfileItem profile = (ProfileItem)args.getSerializable("profile");
        tvFirstName.setText(profile.getFirstName());
        tvLastName.setText(profile.getLastName());
        tvCity.setText(profile.getCity());
        tvCountry.setText(profile.getCountry());
        tvHometown.setText(profile.getHometown());
        tvOccupation.setText(profile.getOccupation());
        tvProfileDescription.setText(profile.getProfileDescription());
    }

    @OnClick(R.id.btn_logout) void onLogoutClicked(){
        client.clearAccessToken();
        mPrefHandler.saveString("nsid", "");
        Toast.makeText(getContext(), "Loggede ud", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getContext(), AuthenticationActivity.class);
        startActivity(intent);
    }
}
