package dk.lundudvikling.flickrphotos;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.drawable.DrawerArrowDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import dk.lundudvikling.flickrphotos.Fragments.MapFragment;
import dk.lundudvikling.flickrphotos.Fragments.MyImagesFragment;
import dk.lundudvikling.flickrphotos.Fragments.MyProfileFragment;
import dk.lundudvikling.flickrphotos.Fragments.PublicImagesFragment;
import dk.lundudvikling.flickrphotos.Handlers.FlickrClientApp;
import dk.lundudvikling.flickrphotos.Handlers.FlickrClientHandler;
import dk.lundudvikling.flickrphotos.Handlers.FragmentHandler;
import dk.lundudvikling.flickrphotos.Handlers.SharedPrefHandler;
import dk.lundudvikling.flickrphotos.Model.ProfileItem;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private boolean isDrawerOpen;
    private FragmentHandler mFragmentHandler;
    @BindView(R.id.drawer_layout) DrawerLayout mDrawerLayout;
    @BindView(R.id.fab_drawer_main) FloatingActionButton mFabDrawer;
    @BindView(R.id.mainView) FrameLayout mMainView;
    @BindView(R.id.nav_view) NavigationView navigationView;
    private FlickrClientHandler client;
    private Dialog mSplashDialog;
    private SharedPrefHandler mPrefHandler;
    private ProfileItem userProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPrefHandler = new SharedPrefHandler(this);
        ButterKnife.bind(this);
        initializeNavigationDrawer();
        mFragmentHandler = new FragmentHandler(this);
        client = FlickrClientApp.getRestClient();
        getUserId();
        PublicImagesFragment fragment = new PublicImagesFragment();
        Bundle args = new Bundle();
        args.putBoolean("shouldProgressShow", false);
        fragment.setArguments(args);
        mFragmentHandler.startTransactionNoAnimationWithBackstack(fragment);
        showSplashScreen();

    }
    public void getUserId(){
        client.testAPI(new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers,
                                  JSONObject json) {
                try {
                    JSONObject profile = json.getJSONObject("user");
                    mPrefHandler.saveString("nsid", profile.getString("id"));
                    //Når nsid er fundet, indlæs profil som skal bruges senere
                    loadProfile();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("debug", e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(MainActivity.this, errorResponse.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initializeNavigationDrawer() {
        navigationView.setNavigationItemSelectedListener(this);
        mDrawerLayout.addDrawerListener(new ActionBarDrawerToggle(this, mDrawerLayout, null, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                isDrawerOpen = false;
                supportInvalidateOptionsMenu();
            }
            public void onDrawerOpened(View drawerView) {
                isDrawerOpen = true;
                supportInvalidateOptionsMenu();
            }
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                mMainView.setTranslationX(slideOffset * drawerView.getWidth());
                mDrawerLayout.bringChildToFront(drawerView);
                mDrawerLayout.requestLayout();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mMainView.setZ(2);
                    mFabDrawer.setZ(1);
                }
                drawerAnimation(slideOffset);
            }
        });

        mFabDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(Gravity.START);
            }
        });
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        switch (id){
            case R.id.nav_public_images:
                PublicImagesFragment fragment = new PublicImagesFragment();
                Bundle args = new Bundle();
                args.putBoolean("shouldProgressShow", true);
                fragment.setArguments(args);
                mFragmentHandler.startTransactionNoAnimationWithBackstack(fragment);
                break;
            case R.id.nav_map:
                //Dette må kunne gøres bedre
                requestLocationPermission();
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                    mFragmentHandler.startTransactionNoAnimationWithBackstack(new MapFragment());
                }
                break;
            case R.id.nav_my_images:
                mFragmentHandler.startTransactionAnimatedWithBackstack(new MyImagesFragment(),R.anim.enter_from_bottom, R.anim.exit_to_top, R.anim.enter_from_top, R.anim.exit_to_bottom);
                break;
            case R.id.nav_my_profile:
                Bundle bundle = new Bundle();
                bundle.putSerializable("profile", userProfile);
                MyProfileFragment profileFragment = new MyProfileFragment();
                profileFragment.setArguments(bundle);
                mFragmentHandler.startTransactionAnimatedWithBackstack(profileFragment,R.anim.enter_from_bottom, R.anim.exit_to_top, R.anim.enter_from_top, R.anim.exit_to_bottom);
                break;
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public void loadProfile() {
        client.getMyProfile(new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(getApplicationContext(), errorResponse.toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers,
                                  JSONObject json) {
                try {
                    JSONObject profile = json.getJSONObject("profile");
                    userProfile = new ProfileItem();
                    userProfile.setFirstName(profile.getString("first_name"));
                    userProfile.setLastName(profile.getString("last_name"));
                    userProfile.setOccupation(profile.getString("occupation"));
                    userProfile.setProfileDescription(profile.getString("profile_description"));
                    userProfile.setCity(profile.getString("city"));
                    userProfile.setHometown(profile.getString("hometown"));
                    userProfile.setCountry(profile.getString("country"));

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("debug", e.toString());
                }
            }
        });
    }

    private void drawerAnimation(final float slideOffset) {
        final DrawerArrowDrawable drawable = new DrawerArrowDrawable(this);
        mFabDrawer.setColorFilter(Color.WHITE);
        mFabDrawer.setImageDrawable(drawable);

        ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                drawable.setProgress(slideOffset);
            }
        });
        if (isDrawerOpen) {
            animator.start();
        } else {
            animator.reverse();
        }
    }

    private void removeSplashScreen() {
        if (mSplashDialog != null) {
            mSplashDialog.dismiss();
            mSplashDialog = null;
        }
    }

    private void showSplashScreen() {
        mSplashDialog = new Dialog(this, R.style.SplashScreenDialog);
        mSplashDialog.setContentView(R.layout.dialog_splash_screen);
        mSplashDialog.setCancelable(false);
        mSplashDialog.show();

        //Hardcoded tid på splash screen skal selvfølgelig ikke være hardcoded.
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                removeSplashScreen();
            }
        }, 2500);
    }

    public void requestLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION
                }, 10);
            }
        }
    }
}
