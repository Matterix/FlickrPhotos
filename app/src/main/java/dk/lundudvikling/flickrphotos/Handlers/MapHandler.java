package dk.lundudvikling.flickrphotos.Handlers;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;

import dk.lundudvikling.flickrphotos.R;

public class MapHandler {
    private static MapHandler mapsHandler;

    //Singleton pattern -->
    public static MapHandler getMapsHandler() {
        if (mapsHandler == null)
            mapsHandler = new MapHandler();
        return mapsHandler;
    }

    public void getMapSettings(Context context, GoogleMap googleMap) {
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.getUiSettings().setTiltGesturesEnabled(false);
        googleMap.setPadding(20, 300, 20, 50);
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.map_style));
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.setMyLocationEnabled(true);
    }

    public void moveCameraTo(GoogleMap googleMap, LatLng endPoint, Float zoomLevel){
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(endPoint.latitude, endPoint.longitude), zoomLevel));
    }

    public void animateCameraTo(GoogleMap googleMap, LatLng endPoint, Float zoomLevel){
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(endPoint.latitude, endPoint.longitude), zoomLevel));
    }
}
