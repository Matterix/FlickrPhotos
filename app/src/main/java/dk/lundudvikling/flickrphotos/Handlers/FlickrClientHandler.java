package dk.lundudvikling.flickrphotos.Handlers;


import android.content.Context;
import android.util.Log;

import com.codepath.oauth.OAuthBaseClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.builder.api.Api;
import org.scribe.builder.api.FlickrApi;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

import dk.lundudvikling.flickrphotos.Model.GalleryItem;

public class FlickrClientHandler extends OAuthBaseClient {

    public static final Class<? extends Api> REST_API_CLASS = FlickrApi.class;

    public static final String REST_URL = "https://www.flickr.com/services";

    public static final String REST_CONSUMER_KEY = "5b446b9c114669e36bc1406b22996926";

    public static final String REST_CONSUMER_SECRET = "2f8635cb8fe24df4";

    public static final String REST_CALLBACK_URL = "oauth://flickrphotoscallback";

    private SharedPrefHandler mPrefHandler;

    public FlickrClientHandler(Context context) {
        super(context, REST_API_CLASS, REST_URL, REST_CONSUMER_KEY, REST_CONSUMER_SECRET,
                REST_CALLBACK_URL);
        setBaseUrl("https://api.flickr.com/services/rest");
        mPrefHandler = new SharedPrefHandler(context);
    }

    public void testAPI(AsyncHttpResponseHandler handler) {
        String apiUrl = getApiUrl("?format=json&nojsoncallback=1&method=flickr.test.login");
        client.get(apiUrl, null, handler);
    }

    public void getPublicImages(AsyncHttpResponseHandler handler){
        String apiUrl = getApiUrl("?per_page=300&format=json&nojsoncallback=1&extras=url_s,url_z&method=flickr.photos.getRecent");
        client.get(apiUrl, null, handler);
    }

    public void getMyImages(AsyncHttpResponseHandler handler){
        String apiUrl = getApiUrl("?format=json&nojsoncallback=1&extras=url_s&method=flickr.activity.userPhotos");
        client.get(apiUrl, null, handler);
    }

    public void getNearbyImages(AsyncHttpResponseHandler handler){
        if (mPrefHandler.getString("lat") == null){
            String apiUrl = getApiUrl("?per_page=300&format=json&nojsoncallback=1&extras=url_s,url_z,geo&lat=55.39&lon=10.43&radius=500&method=flickr.photos.search");
            client.get(apiUrl, null, handler);
        }else{
            Double latCoordinate = Double.parseDouble(mPrefHandler.getString("lat"));
            Double longCoordinate = Double.parseDouble(mPrefHandler.getString("long"));
            String coordinateString = "&lat="+latCoordinate +"&lon="+longCoordinate;
            String apiUrl = getApiUrl("?per_page=300&format=json&nojsoncallback=1&extras=url_s,url_z,geo" + coordinateString +"&radius=5&method=flickr.photos.search");
            client.get(apiUrl, null, handler);
        }
    }

    public void getMyProfile(AsyncHttpResponseHandler handler){
        String userId = mPrefHandler.getString("nsid");
        String apiUrl = getApiUrl("?user_id=" + userId + "&format=json&nojsoncallback=1&method=flickr.profile.getProfile");
        client.get(apiUrl, null, handler);
    }
}
