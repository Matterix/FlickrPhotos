package dk.lundudvikling.flickrphotos.Handlers;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharedPrefHandler {

    private final String MY_PREFS_NAME = "FlickrPhotosPref";
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mEditor;

    public SharedPrefHandler(Context context) {
        mPrefs = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
    }

    public String getString(String key) {
        return mPrefs.getString(key, null);
    }

    public void saveString(String key, String value) {
        mEditor = mPrefs.edit();
        mEditor.putString(key, value);
        mEditor.apply();
    }
}