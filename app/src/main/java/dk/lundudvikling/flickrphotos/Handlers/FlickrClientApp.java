package dk.lundudvikling.flickrphotos.Handlers;

import android.app.Application;
import android.content.Context;

public class FlickrClientApp extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        FlickrClientApp.context = this;
    }

    public static FlickrClientHandler getRestClient() {
        return (FlickrClientHandler) FlickrClientHandler.getInstance(FlickrClientHandler.class, FlickrClientApp.context);
    }
}