package dk.lundudvikling.flickrphotos.Handlers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import java.util.List;

import dk.lundudvikling.flickrphotos.Fragments.BaseFragment;
import dk.lundudvikling.flickrphotos.MainActivity;
import dk.lundudvikling.flickrphotos.R;

public class FragmentHandler {

    private FragmentManager fm;

    public FragmentHandler(MainActivity activity){
        fm = activity.getSupportFragmentManager();
    }

    public void startTransactionNoAnimationWithBackstack(BaseFragment fragment){
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void startTransactionNoBackstack(BaseFragment fragment){
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        transaction.commitAllowingStateLoss();
    }

    public void startTransactionAnimatedWithBackstack(BaseFragment fragment, int startEnter, int startExit, int endEnter, int endExit){
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.setCustomAnimations(startEnter,startExit,endEnter,endExit);
        transaction.replace(R.id.content_frame, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void startTransactionAnimatedNoBackstack(BaseFragment fragment, int startEnter, int startExit, int endEnter, int endExit){
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.setCustomAnimations(startEnter,startExit,endEnter,endExit);
        transaction.replace(R.id.content_frame, fragment);
        transaction.commit();
    }

    public void popBackstack(){
        fm.popBackStack();
    }

    public List<Fragment> getFragments(){
        return fm.getFragments();
    }

}
