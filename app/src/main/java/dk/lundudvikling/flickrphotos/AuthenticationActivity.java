package dk.lundudvikling.flickrphotos;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.codepath.oauth.OAuthLoginActivity;

import dk.lundudvikling.flickrphotos.Handlers.FlickrClientHandler;

public class AuthenticationActivity extends OAuthLoginActivity<FlickrClientHandler> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);
        Button btnFlickrLogin = findViewById(R.id.btn_flickr_login);
        btnFlickrLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getClient().connect();
            }
        });
    }

    @Override
    public void onLoginSuccess() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    @Override
    public void onLoginFailure(Exception e) {
        Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
    }
}
